const isPage = (page) => {
  return window.location.pathname === `/dist/${page}.html`;
};
let cartObject;

if (!localStorage.getItem("cart")) {
  cartObject = {
    items: [],
  };
}

function addCard(place, obj) {
  let price = "";
  if (obj.salePrice) {
    price = `<div class="line-through text-2xl font-light lineheightneg">
        ${obj.originalPrice}:-
      </div>
      <div class="textred text-2xl font-light lineheigtneg">
        ${obj.salePrice}:-
      </div>`;
  } else {
    price = `<div class="text-2xl font-light lineheightneg">
        ${obj.originalPrice}:-
      </div>`;
  }
  const uniqueCardId = Math.floor(Math.random() * 10000000);
  const sizes = obj.sizes.toString().replace(/,/g, " ");
  place.insertAdjacentHTML(
    "beforeend",
    `
    <li class="bgwhiter productcard active" id="product-${uniqueCardId}">
            <div class="oneboxgrid image-size">
              <div class="blurimage"></div>
              <div
                class="z-top self-end text-black w-full bg-whiter show-on-hover p-3 text-xs"
              >
                FINNS I
                <span class="sizes text-sm font-bold"
                  >${sizes}</span
                >
              </div>
              <div class="text-white text-shadow text-xl z-top show-on-hover moreInfo">
                MER INFO
              </div>
              <img src="${obj.imagesrc}" alt="bild på ${obj.title}" class="productimage" />
            </div>
            <div class="px-1 flex flex-col flexer">
              <div class="uppercase">${obj.brand}</div>
              <div
                class="uppercase font-bold lineheightneg"
              >
                ${obj.title}
              </div>
              <div class="spacer"></div>
            </div>
            <div class="flex justify-between last-text">
            <div class="flex gap-2 items-center">
              ${price}
            </div>
            <div class="flex gap-2 items-center">
              <button class="add-favorite">
                <svg
                  width="29"
                  height="25"
                  viewBox="0 0 29 25"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M8.875 2C5.07813 2 2 5.07813 2 8.875C2 15.75 10.125 22 14.5 23.4538C18.875 22 27 15.75 27 8.875C27 5.07813 23.9219 2 20.125 2C17.8 2 15.7438 3.15438 14.5 4.92125C13.8661 4.01825 13.0239 3.28131 12.0447 2.77281C11.0656 2.2643 9.97831 1.99922 8.875 2Z"
                    stroke="#353431"
                    stroke-width="2.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </button>
              <button class="add-cart test-cart" id="buy-item-${uniqueCardId}">
                <svg
                  width="20"
                  height="26"
                  viewBox="0 0 20 26"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M2.5 25.5C1.8125 25.5 1.22417 25.2554 0.735 24.7663C0.245833 24.2771 0.000833333 23.6883 0 23V8C0 7.3125 0.245 6.72417 0.735 6.235C1.225 5.74583 1.81333 5.50083 2.5 5.5H5C5 4.125 5.48958 2.94792 6.46875 1.96875C7.44792 0.989583 8.625 0.5 10 0.5C11.375 0.5 12.5521 0.989583 13.5312 1.96875C14.5104 2.94792 15 4.125 15 5.5H17.5C18.1875 5.5 18.7763 5.745 19.2663 6.235C19.7563 6.725 20.0008 7.31333 20 8V23C20 23.6875 19.7554 24.2763 19.2663 24.7663C18.7771 25.2563 18.1883 25.5008 17.5 25.5H2.5ZM2.5 23H17.5V8H15V10.5C15 10.8542 14.88 11.1513 14.64 11.3913C14.4 11.6313 14.1033 11.7508 13.75 11.75C13.3958 11.75 13.0992 11.63 12.86 11.39C12.6208 11.15 12.5008 10.8533 12.5 10.5V8H7.5V10.5C7.5 10.8542 7.38 11.1513 7.14 11.3913C6.9 11.6313 6.60333 11.7508 6.25 11.75C5.89583 11.75 5.59917 11.63 5.36 11.39C5.12083 11.15 5.00083 10.8533 5 10.5V8H2.5V23ZM7.5 5.5H12.5C12.5 4.8125 12.2554 4.22417 11.7663 3.735C11.2771 3.24583 10.6883 3.00083 10 3C9.3125 3 8.72417 3.245 8.235 3.735C7.74583 4.225 7.50083 4.81333 7.5 5.5Z"
                    fill="#353431"
                  />
                </svg>
              </button>
            </div>
          </div>

          </li>
    
    `
  );
  document
    .querySelector(`#product-${uniqueCardId}`)
    .querySelector(".moreInfo")
    .addEventListener("click", () => {
      localStorage.setItem("clickedCardItem", JSON.stringify(obj));
      window.location = `/dist/productview.html#${obj.id}`;
    });
  let sizeOptions = "";
  obj.sizes.forEach((size) => {
    sizeOptions += `<option value="${size.toUpperCase()}">${size.toUpperCase()}</option>`;
  });
  const button = document.querySelector(`#buy-item-${uniqueCardId}`);
  button.addEventListener("click", () => {
    button.disabled = true;
    const card = document.querySelector(`#product-${uniqueCardId}`);
    card.classList.remove("active");
    card.insertAdjacentHTML(
      "beforeend",
      `<div class="bg-white choose-size flex items-center p-2"> <div class="shadeCard"></div>Välj Storlek <select id="size${uniqueCardId}" class="select-size">${sizeOptions}</select><button id="card-buy${uniqueCardId}" class="card-buy">KÖP</button><button id="card-quit${uniqueCardId}" class="card-quit">X</button></div>`
    );
    const buyButton = card.querySelector(`#card-buy${uniqueCardId}`);

    buyButton.addEventListener("click", () => {
      const value = document.querySelector(`#size${uniqueCardId}`).value;
      let cartContainsItem = false;
      const newObj = {
        item: obj,
        chosenSize: value,
        sizeId: value + obj.id,
        number: 1,
      };

      const cartElements = document.querySelector("#cart-items");
      cartElements.querySelectorAll(".item").forEach((element, index) => {
        if (
          element.querySelector(".articlenumber").textContent ===
          value + obj.id
        ) {
          cartContainsItem = true;
          cartObject.items.forEach((item) => {
            if (item.sizeId === value + obj.id) {
              item.number++;
            }
          });
        }
      });
      card.removeChild(card.lastChild);
      card.insertAdjacentHTML(
        "beforeend",
        `<div class="added">Tillagd i kundvagn</div>`
      );
      setTimeout(() => {
        card.querySelector(".added").remove();
        card.classList.add("active");
        button.disabled = false;
      }, 500);

      const cartNumber = document.querySelector("#cart-number");

      if (!cartContainsItem) {
        cartObject.items.push(newObj);
        addCartCard(document.querySelector("#cart-items"), newObj);
      }
      cartNumber.textContent++;
      cartNumber.classList.add("active");
      updatePrice();
    });
    const quitButton = card.querySelector(`#card-quit${uniqueCardId}`);
    quitButton.addEventListener("click", () => {
      card.removeChild(card.lastChild);
      card.classList.add("active");
      button.disabled = false;
    });
  });
}
const addCartCard = (place, obj) => {
  let price = "";
  if (obj.item.salePrice) {
    price = obj.item.salePrice;
  } else {
    price = obj.item.originalPrice;
  }
  const separator = !isPage("checkout")
    ? `<div class="cart-separator" id="cart-separator-${obj.sizeId}"></div>`
    : "";
  place.insertAdjacentHTML(
    "beforeend",
    `
    <div class="item bg-whiter flex" id="item-${obj.sizeId}">
    <img src="${
      obj.item.imagesrc
    }" alt="bild på ${obj.item.title} i varukorg" class="cart-image" />
<div class="flex flex-col justify-between w-full">

   
   
      <div class="pl-2 py-2">
        <h5 class="uppercase">${obj.item.brand}</h5>
        <h4 class="uppercase font-bold lineheightneg w-fit">
        <a href="/dist/productview.html#${obj.item.id}">
          ${obj.item.title}
        </a>


        <h4 class="uppercase self-center font-light">${obj.chosenSize}</h4>
        <div id="art-number-${obj.sizeId}" class="hidden articlenumber">${
      obj.sizeId
    }</div>
    </div>





    <div class="cart-functions flex item-center justify-between gap-5 w-full pr-5">
    
      <div id="item-${obj.sizeId}-price" class="self-end flex-grow cart-item-price">${price * obj.number}:-</div>

      <div class="flex gap-2 items-center">
      <button id="item-${
        obj.sizeId
      }-decrease"><svg width="10" height="10" viewBox="0 0 5 7" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M4.54164 6.12516L4.54164 0.875158C4.54147 0.822004 4.52681 0.769903 4.49923 0.724461C4.47166 0.679019 4.43221 0.641959 4.38514 0.617268C4.33807 0.592577 4.28516 0.581191 4.2321 0.584337C4.17903 0.587481 4.12784 0.605038 4.08401 0.635116L0.292345 3.26012C0.135136 3.36891 0.135136 3.63082 0.292345 3.73991L4.08401 6.36491C4.12774 6.39529 4.17897 6.41311 4.23212 6.41643C4.28527 6.41974 4.33831 6.40843 4.38548 6.38372C4.43265 6.359 4.47215 6.32184 4.49968 6.27625C4.52721 6.23067 4.54172 6.17841 4.54164 6.12516Z" fill="#353431"/>
        </svg>
        </button>
        <span id="item-${obj.sizeId}-number" class="item-number">${
      obj.number
    }</span>
      <button id="item-${
        obj.sizeId
      }-increase" class="increase"><svg width="10" height="10" viewBox="0 0 5 7" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.458364 0.874842L0.458363 6.12484C0.45853 6.178 0.47319 6.2301 0.500765 6.27554C0.528341 6.32098 0.567786 6.35804 0.614858 6.38273C0.661929 6.40742 0.714843 6.41881 0.767904 6.41566C0.820965 6.41252 0.872163 6.39496 0.915988 6.36488L4.70766 3.73988C4.86486 3.63109 4.86486 3.36918 4.70766 3.26009L0.915989 0.635092C0.872255 0.604707 0.821031 0.586889 0.767881 0.583573C0.714732 0.580257 0.661691 0.59157 0.61452 0.616283C0.567348 0.640996 0.527852 0.678164 0.500321 0.723748C0.472791 0.769332 0.45828 0.821589 0.458364 0.874842Z" fill="#353431"/>
        </svg>
        </button>
      </div>
        <button id="item-${
          obj.sizeId
        }-delete"><svg width="20" height="20" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M14 1H10.5L9.5 0H4.5L3.5 1H0V3H14M1 16C1 16.5304 1.21071 17.0391 1.58579 17.4142C1.96086 17.7893 2.46957 18 3 18H11C11.5304 18 12.0391 17.7893 12.4142 17.4142C12.7893 17.0391 13 16.5304 13 16V4H1V16Z" fill="#FF1655"/>
          </svg>
          </button>
          </div>          
          </div>


          </div>
          ${separator}
         `
  );

  // Papperskorg/delete item från kundvagn
  place
    .querySelector(`#item-${obj.sizeId}-delete`)
    .addEventListener("click", () => {
      place.querySelector(`#item-${obj.sizeId}`).remove();
      if (!isPage("checkout")) {
        place.querySelector(`#cart-separator-${obj.sizeId}`).remove();
        const cartNumber = document.querySelector("#cart-number");
        cartNumber.textContent -= obj.number;
        if (cartNumber.textContent === "0") {
          cartNumber.classList.remove("active");
        }
      }
      cartObject.items.splice(cartObject.items.indexOf(obj), 1);
      updatePrice();
      if (!cartObject.items.length && !isPage("checkout")) {
        closeCart();
      }
    });

  // Justera antal +/- i kundvagn
  place
    .querySelector(`#item-${obj.sizeId}-increase`)
    .addEventListener("click", () => {
      place.querySelector(`#item-${obj.sizeId}-number`).textContent++;
      obj.number++;
      if (!isPage("checkout")) {
        document.querySelector("#cart-number").textContent++;
      }

      place.querySelector(`#item-${obj.sizeId}-price`).textContent = `${
        obj.number * price
      }:-`;
      updatePrice();
    });

  // Justera antal +/- i kundvagn
  place
    .querySelector(`#item-${obj.sizeId}-decrease`)
    .addEventListener("click", () => {
      if (obj.number > 1) {
        place.querySelector(`#item-${obj.sizeId}-number`).textContent--;
        obj.number--;
        if (!isPage("checkout")) {
          document.querySelector("#cart-number").textContent--;
        }
        place.querySelector(`#item-${obj.sizeId}-price`).textContent = `${
          obj.number * price
        }:-`;
        updatePrice();
      }
    });
};

if (!isPage("checkout")) {
  const cartBtn = document.querySelector("#top-cart-button");
  cartBtn.addEventListener("click", () => {
    disableCheckout();
    if (!cartObject.items.length) {
      document
        .querySelector("#cart-items")
        .insertAdjacentHTML(
          "afterbegin",
          `<div class="empty-cart">DIN KUNDVAGN ÄR TOM</div>`
        );
    }
    document.querySelector("#cart-cover").classList.add("active");
    document.querySelector("#cart").classList.add("active");
    document.querySelectorAll(".item").forEach((item, index) => {
      item.querySelector(".cart-item-price").textContent = `${
        cartObject.items[index].number *
        originalOrSalePrice(
          cartObject.items[index].item.salePrice,
          cartObject.items[index].item.originalPrice
        )
      }:-`;
      item.querySelector(".item-number").textContent =
        cartObject.items[index].number;
    });
    updatePrice();
    //
  });
}
const closeCart = () => {
  document.querySelector("#cart-cover").classList.remove("active");
  document.querySelector("#cart").classList.remove("active");
  if (!cartObject.items.length) {
    document
      .querySelector("#cart-items")
      .removeChild(document.querySelector("#cart-items").firstChild);
  }
};
if (!isPage("checkout")) {
  document.querySelector("#cart-cover").addEventListener("click", closeCart);
  document.querySelector("#close-cart").addEventListener("click", closeCart);
}
function originalOrSalePrice(salePrice, originalPrice) {
  return salePrice ? salePrice : originalPrice;
}
function disableCheckout() {
  if (!cartObject.items.length) {
    document.querySelector("#checkout-btn").href = "#";
    document.querySelector("#checkout-btn").style.opacity = ".5";
  } else {
    document.querySelector("#checkout-btn").href = "/dist/checkout.html";
    document.querySelector("#checkout-btn").style.opacity = "1";
  }
}
//cartObject.items[i].item.salePrice rea pris
//cartObject.items[i].item.originalPrice original pris
//cartObject.items[i].chosenSize vald storlek
//cartObject.items[i].number
function updatePrice(discount, shipping) {
  let showPrice = 0;
  let totalItems = 0;
  cartObject.items.forEach((article) => {
    let addPrice =
      article.number *
      originalOrSalePrice(article.item.salePrice, article.item.originalPrice);
    showPrice += addPrice;
    totalItems += article.number;
  });
  if (!isPage("checkout")) {
    document.querySelector(
      "#cart-itemnumber"
    ).textContent = `ANTAL VAROR: ${totalItems} ST`;
    document.querySelector("#cart-price").textContent = `PRIS (EXKL. MOMS): ${
      showPrice * 0.75
    }:-`;
    document.querySelector("#cart-tax").textContent = `VARAV MOMS: ${
      showPrice * 0.25
    }:-`;
    document.querySelector(
      "#cart-price-total"
    ).textContent = `PRIS (TOTAL): ${showPrice}:-`;
  } else {
    if (!discount) {
      discount = 0;
    }
    let showDiscount = "";
    if (discount !== 0) {
      showDiscount = `<span class="text-red font-bold">(${
        discount * 100
      }%)</span>`;
    }
    let shippingAlt;
    if (shipping) {
      shippingAlt = showPrice >= 500 ? 0 : shipping;
    } else {
      shippingAlt = 0;
    }
    document.querySelector(
      "#checkout-price"
    ).textContent = `Pris (inkl. moms): ${showPrice}:-`;
    document.querySelector(
      "#checkout-shipping"
    ).textContent = `Frakt: ${shippingAlt}:-`;
    document.querySelector("#checkout-discount").innerHTML = `Rabatt: ${
      discount * showPrice
    }:- ${showDiscount}`;
    document.querySelector("#checkout-sum").textContent = `Summa (TOTAL): ${
      showPrice + shippingAlt - discount * showPrice
    }:-`;
  }
  localStorage.setItem("cart", JSON.stringify(cartObject));
}
if (!isPage("checkout")) {
  window.onload = () => {
    scrollFunction();
    if (localStorage.getItem("cart")) {
      cartObject = JSON.parse(localStorage.getItem("cart"));
      let totalItems = 0;
      cartObject.items.forEach((article) => {
        totalItems += article.number;
        document.querySelector("#cart-number").classList.add("active");
        addCartCard(document.querySelector("#cart-items"), article);
      });
      document.querySelector("#cart-number").textContent = totalItems;
      updatePrice();
    }
  };
}

const logoHeader = document.querySelector("#header-logo");
const nav = document.querySelector("#nav");

function scrollFunction() {
  if (document.body.scrollTop > 24 || document.documentElement.scrollTop > 24) {
    logoHeader.classList.add("active");
    nav.classList.remove("active");
    if(window.matchMedia("(max-width: 925px)").matches){
    document.querySelector("#hamburgerMeny").classList.add("move")
    }
  } else {
    nav.classList.add("active");
    logoHeader.classList.remove("active");
    if(window.matchMedia("(max-width: 925px)").matches){
    document.querySelector("#hamburgerMeny").classList.remove("move")
    }
  }
}

// keep track of previous scroll position
let prevScrollPos = window.scrollY;
if (!isPage("checkout")) {
  window.addEventListener("scroll", () => {
    // current scroll position
    scrollFunction();
    const currentScrollPos = window.scrollY;

    if (prevScrollPos > currentScrollPos) {
      // user has scrolled up
      nav.classList.add("active");
    } else {
      // user has scrolled down
      nav.classList.remove("active");
    }

    // update previous scroll position
    prevScrollPos = currentScrollPos;
  });
}
if (isPage("index")) {
  fetch("/products.json")
    .then((res) => res.json())
    .then((data) => {
      categories.forEach((category) => {
        addCategory(category);
      });
      data.forEach((item) => {
        if (item.salePrice) {
          addCard(document.querySelector("#itemInputDeals"), item);
        }
        categories.forEach((obj) => {
          if (obj.category === item.category) {
            addCard(document.querySelector(`#itemInput${obj.category}`), item);
          }
        });
      });

      document.querySelectorAll(".expand-previous").forEach((button) => {
        button.addEventListener("click", () => {
          button.previousElementSibling.classList.toggle("active");
          button.classList.toggle("active");
        });
      });
    });
}

const categories = [
  {
    category: "Byxor",
    imagesrc: `/img/categoryimages/pants.jpg`,
    categoryHash: "#Byxor",
  },
  {
    category: "Klänningar",
    imagesrc: `/img/categoryimages/dresses.jpg`,
    categoryHash: "#Kl%C3%A4nningar",
  },
  {
    category: "Tröjor",
    imagesrc: `/img/categoryimages/shirts.jpg`,
    categoryHash: "#Tr%C3%B6jor",
  },
];
const addCategory = (obj) => {
  document.querySelector("#dealSection").insertAdjacentHTML(
    "afterend",
    `<section class="paddingx my-4 category-container">
  <a href="/dist/productview.html${obj.categoryHash}"class="oneboxgrid category-image-container">
    <img
      src="${obj.imagesrc}"
      alt="bild på ${obj.category}"
      class="categoryimage"
    />
    <div class="gradient"></div>
    <div class="text-center">
      <div class="text-xl textwhite">SE ALLA</div>
      <div class="text-brand font-black text-4xl lineheightneg text-shadow">
        ${obj.category.toUpperCase()}
      </div>
    </div>
  </a>
  <ul class="textblack gap-4 category-ul" id="itemInput${
    obj.category
  }"></ul>
  <div class="expand-previous"><button><svg width="18" height="10" viewBox="0 0 70 40" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M68.4592 1.5296C67.4723 0.550205 66.134 7.95375e-06 64.7386 7.83176e-06C63.3431 7.70977e-06 62.0048 0.550204 61.0179 1.5296L34.9682 27.3897L8.91853 1.5296C7.926 0.577956 6.59666 0.0513792 5.21684 0.0632822C3.83701 0.0751851 2.51709 0.624615 1.54137 1.59324C0.565645 2.56186 0.0121924 3.87217 0.000201985 5.24196C-0.0117885 6.61174 0.518648 7.9314 1.47727 8.91671L31.2476 38.4704C32.2345 39.4498 33.5728 40 34.9682 40C36.3637 40 37.702 39.4498 38.6889 38.4704L68.4592 8.91672C69.4458 7.93702 70 6.60845 70 5.22316C70 3.83787 69.4458 2.5093 68.4592 1.5296Z" fill="#FFA3BC"/>
    </svg>
    </button>
    </div>
</section>`
  );
};

if (isPage("checkout")) {
  const customerObject = {
    fName: "",
    lName: "",
    email: "",
    phoneNumber: "",
    adress: "",
    postCode: "",
    city: "",
  };
  document.querySelector("#header-logo").classList.add("active");
  cartObject = JSON.parse(localStorage.getItem("cart"));
  cartObject.items.forEach((item) => {
    addCartCard(document.querySelector("#checkoutproducts"), item);
  });
  updatePrice();

  let prevIndex = 0;
  document.querySelectorAll(".shipping-category").forEach((button, index) => {
    button.addEventListener("click", () => {
      button.nextElementSibling.classList.toggle("active");
      button.classList.toggle("active");
      document.querySelectorAll(".shipping-category")[prevIndex].click();
      prevIndex = index;
    });
  });

  document.querySelectorAll(".nextBtn").forEach((button, index) => {
    const sectionList = document.querySelectorAll(".checkoutSection");
    button.addEventListener("click", () => {
      sectionList[index].classList.remove("active");
      sectionList[index].inert = true;
      if (index < sectionList.length - 1) {
        sectionList[index + 1].classList.add("active");
        sectionList[index + 1].inert = false;
        document
          .querySelectorAll(".nextBtn")
          [index].scrollIntoView({ behavior: "smooth", block: "start" });
      }
      if (index === sectionList.length - 1) {
        document.querySelector(".total").classList.add("active");
        document.querySelector(".rightside").classList.add("active");
        document.querySelector("#goBack").classList.add("active");
        document.querySelector("#confirmPurchase").disabled = false;
        document.querySelector("#goBack").addEventListener("click", () => {
          document.querySelector(".total").classList.remove("active");
          document.querySelector("#goBack").classList.remove("active");
          document.querySelector(".rightside").classList.remove("active");
          sectionList[sectionList.length - 1].classList.add("active");
          sectionList[sectionList.length - 1].inert = false;
          document.querySelector("#confirmPurchase").disabled = true;
        });
      }

      document.querySelectorAll(".circle")[index].classList.add("active");
    });
  });

  document.querySelectorAll(".backBtn").forEach((button, index) => {
    const sectionList = document.querySelectorAll(".checkoutSection");
    button.addEventListener("click", () => {
      sectionList[index].classList.remove("active");
      sectionList[index].inert = true;
      if (index > 0) {
        sectionList[index - 1].classList.add("active");
        sectionList[index - 1].inert = false;
        document
          .querySelectorAll(".backBtn")
          [index - 1].scrollIntoView({ behavior: "smooth", block: "end" });
      }
      if (index === 0) {
        window.location = "/dist/index.html";
      }
      document.querySelectorAll(".circle")[index].classList.remove("active");
    });
  });

  document.querySelectorAll(".input").forEach((field) => {
    field.addEventListener("focusout", () => {
      field.nextElementSibling.classList.add("active");
      if (field.value === "") {
        field.classList.add("wrong");
        field.classList.remove("right");
        field.nextElementSibling.innerText = "✘";
        field.nextElementSibling.classList.add("wrong");
        field.nextElementSibling.classList.remove("right");
      } else {
        field.classList.add("right");
        field.classList.remove("wrong");
        field.nextElementSibling.innerText = "✔";
        field.nextElementSibling.classList.add("right");
        field.nextElementSibling.classList.remove("wrong");
      }
    });
  });

  document.querySelector("#discount").addEventListener("click", () => {
    const discountCodes = [
      { code: "bingbong", discount: 0.2 },
      { code: "konini50", discount: 0.5 },
    ];
    discountCodes.forEach((discountCode) => {
      if (
        document.querySelector("#inputDiscount").value === discountCode.code
      ) {
        updatePrice(discountCode.discount);
      }
    });
  });
  document.querySelector("#customerForm").addEventListener("submit", (e) => {
    e.preventDefault();

    let allFilledIn = true;
    const allInputs = document.querySelectorAll(".input");

    for (let i = 0; i < allInputs.length; i++) {
      if (allInputs[i].value === "") {
        allFilledIn = false;
        allInputs[i].focus();
        break;
      }
    }

    if (allFilledIn) {
      document.querySelectorAll(".input").forEach((input) => {
        input.classList.add("right");
      });
      customerObject.fName = e.target.fName.value;
      customerObject.lName = e.target.lName.value;
      customerObject.email = e.target.email.value;
      customerObject.phoneNumber = e.target.phonenumber.value;
      customerObject.adress = e.target.adress.value;
      customerObject.postCode = e.target.postcode.value;
      customerObject.city = e.target.city.value;
      document.querySelector("#customerForm").inert = true;
      document.querySelector("#customerForm").classList.add("lockedIn");
      document.querySelector("#openDelivery").disabled = false;
      document.querySelector("#editBtn").classList.add("active");
      document.querySelector("#editBtn").addEventListener("click", () => {
        document.querySelector("#customerForm").inert = false;
        document.querySelector("#customerForm").classList.remove("lockedIn");
        document.querySelector("#openDelivery").disabled = true;
        document.querySelector("#editBtn").classList.remove("active");
      });
    }
  });
  document.querySelector("#discount-form").addEventListener("submit", (e) => {
    e.preventDefault();
    document.querySelector("#inputDiscount").value = "";
  });

  document.querySelectorAll(`[name="shipping-choice"]`).forEach((radio) => {
    radio.addEventListener("change", () => {
      document.querySelector("#klarBtn").disabled = false;
      document.querySelector(
        "#shippingText"
      ).innerText = `Du har valt ${radio.id.toUpperCase()} som fraktalternativ.`;
      updatePrice(0, Number(radio.value));
    });
  });

  const confirmPurchase = document.querySelector("#confirmPurchase");
  //ändra denna till
  confirmPurchase.disabled = true;

  confirmPurchase.addEventListener("click", () => {
    let cartNames = "";
    cartObject.items.forEach((product) => {
      cartNames +=
        " <br>" +
        product.item.title +
        " " +
        product.chosenSize +
        " x" +
        product.number;
    });
    let customer = "";
    for (const [key, value] of Object.entries(customerObject)) {
      customer += `${value} `;
    }

    document.body.insertAdjacentHTML(
      "beforeend",
      `<div class="text center checkoutDone">
       ${customerObject.fName} ${customerObject.lName}
      Tack för ditt köp! Du har beställt ${cartNames}.
      En orderbekräftelse skickas till din e-postadress (${customerObject.email}).
      </div>
      <div class="litendiv">${customer}</div>
      `
    );
  });
}

function addProductView(obj) {
  document.querySelector(".sidemenu-section").classList.add("hide")
  let price = obj.originalPrice + ":-";
  if (obj.salePrice) {
    price = `<span class="line-through">${obj.originalPrice}:-</span> <span class="text-red">${obj.salePrice}:-</span>`;
  }

  let sizeButtons = "";
  obj.sizes.forEach((size) => {
    sizeButtons += `<button class="sizeButtonsProductpage" id="${size}">${size.toUpperCase()}</button>`;
  });
  let smallerImages = ` <img
  src="${obj.imagesrc}"
  class="product-img"
  alt="bild på ${obj.title}"
/>`;
  obj.smallerImages.forEach((image) => {
    smallerImages += ` <img
  src="${image}"
  class="product-img"
  alt="alternativ bild på ${obj.title}"
/>`;
  });
  document.querySelector(".products").insertAdjacentHTML(
    "beforeend",
    `
  <section class="productview-section">
            <div class="flex flex-col gap-3">
              ${smallerImages}
            </div>
            <div class="product-hero-container">
              <img
                src="${obj.imagesrc}"
                class="product-hero-img"
                alt="bild på ${obj.title}"
              />
            </div>
          </section>
  
          <section class="product-info grid m-4">
            <div class="productinfo-head flex flex-col">
              <p class="producttext-brand uppercase font-medium text-sm">
                ${obj.brand}
              </p>
              <p class="text-black font-bold uppercase text-2xl">
                ${obj.title}
              </p>
              <diV class="cart-separator"></div>
              <p class="text-4xl font-light">${price}</p>
              <div class="chooseTheSize relative price mt-5 uppercase"> Välj Storlek</div>
              <div class="flex gap-2 font-bold">
                ${sizeButtons}
              </div>
              
              <div class="buy-like buttons-wrapper flex items-center">
                <button id="buyBtn" disabled class="buy-product-btn bg-brand px-2.5 py-1 my-4 mr-3">
                  KÖP
                </button>
                <button class="like-product-btn">
                  <svg
                    width="29"
                    height="25"
                    viewBox="0 0 29 25"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8.875 2C5.07813 2 2 5.07813 2 8.875C2 15.75 10.125 22 14.5 23.4538C18.875 22 27 15.75 27 8.875C27 5.07813 23.9219 2 20.125 2C17.8 2 15.7438 3.15438 14.5 4.92125C13.8661 4.01825 13.0239 3.28131 12.0447 2.77281C11.0656 2.2643 9.97831 1.99922 8.875 2Z"
                      stroke="#353431"
                      stroke-width="2.5"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                </button>
              </div>
            </div>
            <div class="product-info-body max-w-md">
              <h6 class="uppercase text-xl">Beskrivning</h6>
              <p class="font-serif">
               ${obj.description}
              </p>
            </div>
            <diV class="cart-separator"></div>
            <section class="product-info-lower grid-cols-1">
              <ul class="product-info-list flex flex-col">
                <li>Detaljer</li>
                <li>Material och Skötsel</li>
                <li>Leverans & Retur</li>
                <li>Hållbarhet</li>
                <li>Recensioner</li>
              </ul>
            </section>
          </section>
        </div>
     `
  );

   const imagesHeroAndSmall = {
     mainImage: obj.imagesrc,
     smallImages: obj.smallerImages
   }
  
   const mainImageElement = document.querySelector(".product-hero-img");
   mainImageElement.src = imagesHeroAndSmall.mainImage;
   const smallImageElements = document.querySelectorAll(".product-img")
  
   smallImageElements.forEach((smallImage) =>
    {
     smallImage.addEventListener("click", function () {
       mainImageElement.src = smallImage.src;
     })
    })
   

  const buttons = document.querySelectorAll(".sizeButtonsProductpage");
  let chosenSize;

  buttons.forEach((button, index) => {
    button.addEventListener("click", () => {
      for (let i = 0; i < buttons.length; i++) {
        if (i !== index) {
          buttons[i].classList.remove("active");
        }
      }
      chosenSize = button.innerText;
      button.classList.add("active");
      document.querySelector("#buyBtn").disabled = false;
    });
  });

  document.querySelector("#buyBtn").addEventListener("click", () => {
    const value = chosenSize;
    const newObj = {
      item: obj,
      chosenSize: value,
      sizeId: value + obj.id,
      number: 1,
    };
    document
      .querySelector(".price")
      .insertAdjacentHTML(
        "beforeend",
        `<div class="added">Tillagd i kundvagn</div>`
      );
    setTimeout(() => {
      document.querySelector(".price").querySelector(".added").remove();
    }, 700);
    let cartContainsItem = false;
    const cartNumber = document.querySelector("#cart-number");
    if (!cartObject.items.length) {
      cartObject.items.push(newObj);
      addCartCard(document.querySelector("#cart-items"), newObj);
      cartNumber.textContent++;
      cartNumber.classList.add("active");
      updatePrice();
      return;
    }
    const cartElements = document.querySelector("#cart-items");
    cartElements.querySelectorAll(".item").forEach((element, index) => {
      if (
        element.querySelector(".articlenumber").textContent ===
        value + obj.id
      ) {
        cartObject.items.forEach((item) => {
          if (item.sizeId === value + obj.id) {
            item.number++;
            cartContainsItem = true;
          }
        });
      }
    });
    if (!cartContainsItem) {
      cartObject.items.push(newObj);
      addCartCard(cartElements, newObj);
    }
    cartNumber.textContent++;
    cartNumber.classList.add("active");
    updatePrice();
  });
}

/*
 ************************* CHECKOUT SIDAN NEDANFÖR *****************************
 */
 const buildLink = (link, categoryObj, obj)=>{
  let base = `<a href="#" class="productLink">ALLA KLÄDER</a>`
  if(link===""){
    return base;
  }
  if(link==="#deals"){
    return base+=` / <a href="#deals" class="productLink">DEALS</a>`
  }
  if(link===categoryObj.categoryHash){
    return base+=` / <a href="${categoryObj.categoryHash}" class="productLink">${categoryObj.category}</a>`
  }
  return base+=` / <a href="#${obj.category}" class="productLink">${obj.category}</a> / <a href="#${obj.id}" class="productLink">${obj.title}</a>`
}
if (isPage("productview")) {
  if (localStorage.getItem("cart")) {
    cartObject = JSON.parse(localStorage.getItem("cart"));
  }
  if(window.location.hash===""){
    document.querySelector(".product-page-category").innerHTML=buildLink(window.location.hash)
  }
  if(window.location.hash==="#deals"){
    document.querySelector(".product-page-category").innerHTML=buildLink(window.location.hash)
    }
  window.addEventListener("popstate", () => {
    location.reload();
  });
  console.log(window.location);
  //addProductView(JSON.parse(localStorage.getItem("clickedCardItem")));
  fetch("/products.json")
    .then((res) => res.json())
    .then((data) => {
      data.forEach((item) => {
        if (`#${item.id}` === window.location.hash) {
          document.querySelector(".product-page-category").innerHTML=buildLink(window.location.hash,item.category,item)
          addProductView(item);
        }
        if (window.location.hash === "") {
          addCard(document.querySelector(".products"), item);
        }
        categories.forEach((cat) => {
          if (
            cat.categoryHash === window.location.hash &&
            item.category === cat.category
          ) {
            document.querySelector(".product-page-category").innerHTML=buildLink(window.location.hash,cat)
            addCard(document.querySelector(".products"), item);
          }
          })
          if(window.location.hash==="#deals"){
            if(item.salePrice){
              addCard(document.querySelector(".products"), item);
            }}
        });
        
    });
}
if (!isPage("checkout")) {
document.querySelector(".logInBtn").addEventListener("click", openLogin)
}
function openLogin(){
  if(document.querySelector("#logIn").innerHTML==""){
    document.querySelector("#logIn").insertAdjacentHTML("beforeend",`
    <div class="logInBg"></div>
  <div class="logInDiv">
    <section class="logInSide">
    </section>
    <section class="registerSide">
  
  </section>
  <button class="closeLogIn">X</button>
  </div>
    `)

    createlogInSide()
    document.querySelector(".logInBg").addEventListener("click",closeLogIn)
    document.querySelector(".closeLogIn").addEventListener("click",closeLogIn)
  }
    else{ closeLogIn()}
  
  };
function createRegister(){
  document.querySelector(".registerSide").classList.add("active")
  document.querySelector(".logInSide").classList.remove("active")
  document.querySelector(".registerSide").innerHTML=`<h3>Registrera konto</h3>
  <form action="" class="flex flex-col">
    <label for="email">E-post adress:</label>
    <input type="email" name="email" placeholder="Email@email.com">
    <label for="password">Lösenord:</label>
    <input type="password" name="password" placeholder="Lösenord">
    <label for="upprepaPassword">Upprepa lösenord:</label>
    <input type="password" name="upprepaPassword" placeholder="Upprepa lösenord">
    <button type="submit" class="submitBtn">Registrera</button>
  </form>`
  document.querySelector(".logInSide").innerHTML=`
  <div>Logga in?</div>
  <button class="switch">Klicka här</button>`
  document.querySelector(".switch").addEventListener("click",()=>{
    createlogInSide()
  })

}
function createlogInSide(){
  document.querySelector(".logInSide").classList.add("active")
  document.querySelector(".registerSide").classList.remove("active")
document.querySelector(".logInSide").innerHTML=`
<h3 class="">Logga in</h3>
<form action="" class="flex flex-col">
<label for="email">E-post adress:</label>
<input type="email" name="email" placeholder="Email@email.com">
<label for="password">Lösenord:</label>
<input type="password" name="password" placeholder="Lösenord">
<button type="submit" class="submitBtn">Logga In</button>
</form>`

document.querySelector(".registerSide").innerHTML=`<div>Inte Medlem?</div>
<button class="switch">Klicka här</button>`

document.querySelector(".switch").addEventListener("click",()=>{
  createRegister()
})
}
function closeLogIn(){
  document.querySelector("#logIn").innerHTML =""
}
// förstora productview-section,
if(!isPage("checkout")){
const mediaQuery = window.matchMedia("(max-width: 925px)");
if(mediaQuery.matches){
  createHamburgerNav()
}
else{
  document.querySelector("#hamburgerSection").innerHTML =""
  document.querySelector("#hamburger-bg").classList.remove("active")
}
mediaQuery.addEventListener("change",()=>{
if(document.querySelector("#hamburgerSection").innerHTML ==``){
  createHamburgerNav()
}
else{
  document.querySelector("#hamburgerSection").innerHTML =""
  document.querySelector("#hamburger-bg").classList.remove("active")
}
})


const mediaQuery525 = window.matchMedia("(max-width: 525px)");
if(mediaQuery525.matches){
  insertButtonsInNav();
}

mediaQuery525.addEventListener("change",()=>{
  if(!document.querySelector("#hamburgerButtons")){
 insertButtonsInNav()
  }else{
    document.querySelector(".insertButtons").innerHTML =""
    
  }

})



}


function createHamburgerNav(){
  document.querySelector("#hamburgerSection").insertAdjacentHTML("beforeend", `
  <div id="hamburgerMeny">
        <div class="hamburgerMenyBar"></div>
        <div class="hamburgerMenyBar"></div>
        <div class="hamburgerMenyBar"></div>
      </div>
      <nav id="hamburgerNavMeny">
      <div class="insertButtons mb-10"></div>
        <ul class="flex flex-col items-center gap-20 textwhite font-normal">
          <li class="textred font-bold text-shadow text-2xl lineheightneg">
            DEALS
          </li>
          <li>VARUMÄRKEN</li>
          <li>KOLLEKTIONER</li>
          <li>KATEGORIER</li>
        </ul>
      </nav>
  `)
  const hamburger = document.querySelector("#hamburgerMeny")
  const hamburgerNav = document.querySelector("#hamburgerNavMeny")
  hamburger.addEventListener("click",()=>{
    closeLogIn();
    hamburger.classList.toggle("active") 
    document.querySelector("#hamburger-bg").classList.toggle("active")
    hamburgerNav.classList.toggle("active")
  })
  document.querySelector("#hamburger-bg").addEventListener("click",()=>{
    hamburger.classList.remove("active") 
    document.querySelector("#hamburger-bg").classList.remove("active")
    hamburgerNav.classList.remove("active")
  })
}

function insertButtonsInNav(){
  document.querySelector(".insertButtons").insertAdjacentHTML("afterbegin",`
  <div class="flex flex-col gap-8" id="hamburgerButtons">
  <button class="items-center menugrid logInBtn logInHamburger">
    <svg
      width="29"
      height="29"
      viewBox="0 0 29 29"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M8.33333 6.79167C8.33333 5.15616 8.98303 3.58765 10.1395 2.43117C11.296 1.2747 12.8645 0.625 14.5 0.625C16.1355 0.625 17.704 1.2747 18.8605 2.43117C20.017 3.58765 20.6667 5.15616 20.6667 6.79167C20.6667 8.42717 20.017 9.99568 18.8605 11.1522C17.704 12.3086 16.1355 12.9583 14.5 12.9583C12.8645 12.9583 11.296 12.3086 10.1395 11.1522C8.98303 9.99568 8.33333 8.42717 8.33333 6.79167ZM8.33333 16.0417C6.28896 16.0417 4.32831 16.8538 2.88272 18.2994C1.43713 19.745 0.625 21.7056 0.625 23.75C0.625 24.9766 1.11228 26.153 1.97963 27.0204C2.84699 27.8877 4.02337 28.375 5.25 28.375H23.75C24.9766 28.375 26.153 27.8877 27.0204 27.0204C27.8877 26.153 28.375 24.9766 28.375 23.75C28.375 21.7056 27.5629 19.745 26.1173 18.2994C24.6717 16.8538 22.711 16.0417 20.6667 16.0417H8.33333Z"
        fill="#fffffa"
      />
    </svg>

    <div class="text-whiter">Logga in</div>
  </button>

  <button class="flex flex-col items-center menugrid">
    <svg
      width="31"
      height="28"
      viewBox="0 0 31 28"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M15.5 28L13.2525 25.9858C5.27 18.8599 0 14.145 0 8.39237C0 3.67738 3.751 0 8.525 0C11.222 0 13.8105 1.23597 15.5 3.17384C17.1895 1.23597 19.778 0 22.475 0C27.249 0 31 3.67738 31 8.39237C31 14.145 25.73 18.8599 17.7475 25.9858L15.5 28Z"
        fill="#fffffa"
      />
    </svg>

    <div class="text-whiter">Favoriter</div>
  </button>
</div>
  
  `)

  document.querySelector(".logInHamburger").addEventListener("click", ()=>{
    openLogin()
    document.querySelector("#hamburgerMeny").classList.remove("active") 
    document.querySelector("#hamburger-bg").classList.remove("active")
    document.querySelector("#hamburgerNavMeny").classList.remove("active")
  })
}