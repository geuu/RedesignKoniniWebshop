/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./dist/*.{html,js}"],
  theme: {
    fontFamily: {
      brand:['Josefin Sans', 'sans-serif'],
      serif:['Inria Serif', 'serif']
    },
    colors:{
      'brand': '#FFA3BC',
      'primary': '#AA8C94',
      'white': '#FFFEF0',
      'black': '#353431',
      'red': '#FF1655',
      'whiter': '#FFFFFA',
    },
    extend: {
    },
  },
  plugins: [],
}

